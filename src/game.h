/****************************************************************
 * header file for the game
 * by Wil
 * License CC BY 4.0
 ****************************************************************/

#ifndef _GAME_H_

#define _GAME_H_

#include <stdint.h>     // uint*_t
                        

/* --------------------------------------------------
   all functions that are defined after they are 
   called need to be declared here
   so that the compiler knows how they are called
   -------------------------------------------------- */

void gameloop(void);
void endscreen(void);

#endif