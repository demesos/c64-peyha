; -------------------------------------------------------------------------
; Code to set up the sprs (color, costume, initial position)
; -------------------------------------------------------------------------

.include "lamalib.inc"
.include "presets.inc"

.export _init_sprites
.importzp _playerx,_playery

playerx=_playerx
playery=_playery

_init_sprites:
        lda #PLAYERSTARTX/2
        sta playerx
        lda #PLAYERSTARTY
        sta playery
        
	;colors
	lda #GAMESPRCOLOR
	sta $d027
	lda #SPRMUCO1
	sta $d025
	lda #SPRMUCO2
	sta $d026

	;set sprite multicolor
	lda #1
	sta $d01c

	;enable sprite
	;lda #1   ;already set
	sta $d015

	;we don't tamper with spr priority and expansion, because we assume the user
	;loads the game after a clean reset

	rts