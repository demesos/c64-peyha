;********************************************************** 
;* Sound effects for chiseling and explosion
;*
;*
;* Jun-2020 V0.1
;* Wilfried Elmenreich
;* Code under CC-BY-4.0 license
;**********************************************************/

.export _sfx_chiseling_data,_sfx_explosion_data

_sfx_chiseling_data:
  .byte $00,$F5,$08,$C6,$81,$CE,$41,$DD,$40,$00

_sfx_explosion_data:
  .byte $00,$FB,$08,$B8,$81,$A4,$41,$A0,$B4,$81,$98,$92,$9C,$90,$95,$9E
  .byte $92,$80,$94,$8F,$8E,$8D,$8C,$8B,$8A,$89,$88,$87,$86,$84,$00