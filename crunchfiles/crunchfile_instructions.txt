Instructions for adding crunched files to your project

* every file added to this directory will be crunched with exomizer and added to a library crunchedfiles.lib
* the build script links crunchedfiles.lib with cour project
* the label to refer to a crunched file is the filename, where dots are replaced by underscores, e.g., level-1.hu.prg becomes level_1_hu_prg
* use the decrunch function from utilities.h to access and decompress your data, e.g. decrunch(level_1_hu_prg) or decrunch(level_1_hu_prg, 0x400)

Points to remember:
* don�t use spaces in filenames
* the linking is dynamic, if you don�t refer to a label of a file, this file�s data is not added to the final .PRG
* you cannot have the same file name in crunchfiles and linkfiles



