.include "lamalib.inc"
.include "presets.inc"

.zeropage
_map_idx: .res 1

.exportzp _map_idx

.export _get_mapidx
.importzp _playerx,_playery

playerx=_playerx
playery=_playery

.code

_get_mapidx:
	lda playery 
	sec
	sbc #38
	and #%11110000 ;mask out lower bits
	sta sm1+1 ;add this value later
	lsr ;/2
	lsr ;/2
sm1:	adc #00	  
	sta _map_idx
	lda playerx
	lsr ;/2
	lsr ;/2
	lsr ;/2
	clc
	adc _map_idx
	sbc #00	        ;subtract 1
	sta _map_idx	;_map_idx contains now the position in map coordinates
	rts 
