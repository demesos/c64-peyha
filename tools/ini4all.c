/* **************************************************************
 * ini4all -- .INI file converter to other programming languages
 *
 * parses an ini file and writes the values into 
 * .h file for C
 * .inc file for Assembler
 * .bat file for Windows/DOS Shellscripts
 * 
 * Dec-2020 V0.1
 * Wilfried Elmenreich
 * Code under CC-0 "license"
 * **************************************************************/

#define VERSIONSTRING "V0.1 December 2020"
 
// --------------------------------------------------------------
// Definitions and constants
// normally this would be in a separate .h file but since this is
// a single file tool, we keep the code in a single file as well.
// --------------------------------------------------------------
 
#define MAXLINELEN 1024
#define MAXNAMELEN 64

#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <getopt.h>
#include <stdarg.h>
#include <limits.h>

#define TYPE_INT 1
#define TYPE_FLOAT 2
#define TYPE_STRING 3

#define DEBUG(x) printf("DBG: %s",x);

typedef struct {
  char *varname;
  int type;
  union {
    int valInt;
    char *valStr;
    float valFloat;
  } value;
} entry_t;

char line[MAXLINELEN+3];

int no_entries=0;
int linenr=0;
int VERBOSE=0;
char *progname;

FILE *fp_A=0,*fp_B=0,*fp_C=0;

int opt_A=0;
int opt_B=0;
int opt_C=0;
  
int surpress_A=0;
int surpress_B=0;
int surpress_C=0;

// --------------------------------------------------------------
// Implementation of parser and converter
// Unless there are bugs, there should be no need to change 
// anything beyond this comment
// --------------------------------------------------------------

void usage(char *s) {
   if (s!=NULL) fprintf(stderr,"%s: %s\n\n",progname,s);
	
   fprintf(stderr,"Usage: %s [-v] [-?] [-ABC] [-o <outfilename>] <inifilename>\n",progname);

   fprintf(stderr,"\n -A generate an .inc file to be used with assembler\n"); 
   fprintf(stderr,"    all non-integers are export as .define statement (ca65 syntax)\n");    
   fprintf(stderr," -B generate a batch file to be called from for Windows/DOS Shellscripts\n");
   fprintf(stderr," -C generate an .h-file with #define statements to be included in C/C++\n");          
   fprintf(stderr," -o specifiy dir and filename for output. The tool appends the standard\n");        
   fprintf(stderr,"    extensions to the specified filename. If no output filename is\n");      
   fprintf(stderr,"    defined, the name of the inifile without the extension is used.\n");             
   fprintf(stderr," -? usage instructions for this tool\n");
   fprintf(stderr," -V enable verbose output\n"); 
   fprintf(stderr," -v display version information\n"); 
   fprintf(stderr,"\nExample usage: %s -ABC config.ini\n"); 
        
   exit(1);
}

//prints an error message to stderr and exits program
//usage like printf(..), but error_exit does the "\n" for you
void error_exit(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    fprintf(stderr,"%s: ",progname);
    vfprintf(stderr,fmt, args);
    va_end(args);    
    fprintf(stderr,"\n");    
    exit(1);
}

char *allocString(int length) {
  char *s;
  if ((s=(char *)malloc((length+1)*sizeof(char)))==NULL)
    fprintf(stderr,"%s: out of memory error.\n\n",progname);
  return s;
}

char *strn0cpy( char *dest, const char *src, size_t num ) {
  strncpy(dest,src,num);
  dest[num]='\0';
  return dest;
}

int is_eol(char *pos) {
  if ((*pos=='\0') || (*pos=='\r') || (*pos=='\n')) return 1;
  if ((*pos==';') || (*pos=='#')) return 1;
  return 0;
}

char *skipwhitespace(char **pos) {
   while (**pos!='\0') {
     if ((**pos==' ') || (**pos=='\t')) {
       (*pos)++;
     }
     else break;
   } 
   return *pos;
}

char *parsevarname(char **pos) {
   int len=0;
   char *varname;

   while(((*pos)[len]!='=') && ((*pos)[len]!=' ') && (**pos!='\0')) {
     len++;
   }
   if (**pos=='\0') error_exit("Equal sign missing in line %d\n%s",linenr,line);
   varname=allocString(len);
   strn0cpy(varname,*pos,len);
   return varname;
}

char *parsePastEqual(char **pos) {
   skipwhitespace(pos);
   while((**pos!='=') && (**pos!='\0')) {
     (*pos)++;
   } 
   if (**pos=='\0') error_exit("Equal sign missing in line %d\n%s",linenr,line);
   (*pos)++;
   skipwhitespace(pos);
   if (**pos=='\0') error_exit("Equal sign missing in line %d\n%s",linenr,line);
   return *pos;
}

parseInt(char *pos, int *valInt) {
  long val;
  char *endptr;
  int base=10;
  
  if (*pos=='$') {
    base=16;
    pos++;
  } else if (*pos=='%') {
    base=16;
    pos++;  
  } else if (!strncmp(pos,"0x",2)) {
    base=16;
    pos+=2;  
  } else if (!strncmp(pos,"0b",2)) {
    base=2;
    pos+=2;  
  }
  val = strtol(pos, &endptr, base);
  skipwhitespace(&endptr);

  if (is_eol(endptr)) {
    if (val>INT_MAX) error_exit("Integer exceeds maximum integer value in line %d\n%s",linenr,line);
    *valInt=(int)val;
    return 1;
  }
  return 0;
}

parseFloat(char *pos, float *valFloat) {
  float val;
  char *endptr;

  val = strtof(pos, &endptr);
  skipwhitespace(&endptr);

  if (is_eol(endptr)) {
    *valFloat=val;
    return 1;
  }
  return 0;
}

parseString(char *pos, char **valStr) {
  int len;
  char *endptr;

  if (*pos=='"') {
    len=1;
    while((pos[len]!='"') && (pos[len]!='\0')) 
      len++;
    if (pos[len]=='\0') error_exit("String not closed in line %d\n%s",linenr,line);
    endptr=pos+len+1;
    skipwhitespace(&endptr);
    if (!is_eol(endptr))
      return 0;
    *valStr=allocString(len);
    strn0cpy(*valStr,pos,len+1);
    return 1;   
  } else {
    len=-1;
    do {
      len++;
      char *testendptr;
      while((pos[len]!=' ') && (!is_eol(pos+len))) {
        len++;
      }
      endptr=pos+len;
      skipwhitespace(&endptr);
    } while(!is_eol(endptr));
    
    if (!is_eol(endptr)) {   
      if (VERBOSE) printf("%s: Warning: You have to put quotes around strings containing spaces!\n",progname); 
      return 0;
    }
    *valStr=allocString(len);
    strn0cpy(*valStr,pos,len);
    return 1;           
  }
  return 0;
}

int parseentry(char *pos, int *valInt, char **valStr, float *valFloat) {
  
  if (parseInt(pos, valInt)) {
    return TYPE_INT;
  }
  if (parseFloat(pos, valFloat)) {
    return TYPE_FLOAT;
  }
  if (parseString(pos, valStr)) {
    return TYPE_STRING;
  }  
  error_exit("Can't parse expression in line %d\n%s",linenr,line);
}

void write_A(entry_t entry) {
  if (surpress_A || (opt_A==0)) return;
  switch(entry.type) {
      case TYPE_INT:
        fprintf(fp_A,"%s = $%X\n",entry.varname, entry.value.valInt);
        break;      
      case TYPE_STRING:
        if ((entry.value.valStr[0] != '\"') && strchr(entry.value.valStr,'.')) {
           if (VERBOSE) printf("Warning: Added quotes around string %s for Assembler include file.\n",entry.value.valStr);
           fprintf(fp_A,".define %s \"%s\"\n",entry.varname, entry.value.valStr);
        } else
           fprintf(fp_A,".define %s %s\n",entry.varname, entry.value.valStr);
        break;      
      case TYPE_FLOAT:            
        fprintf(fp_A,".define %s %g\n",entry.varname, entry.value.valFloat);
        break;     
    }
}

void write_B(entry_t entry) {
  if (surpress_B || (opt_B==0)) return;
  switch(entry.type) {
      case TYPE_INT:
        fprintf(fp_B,"set %s=%d\n",entry.varname, entry.value.valInt);
        break;      
      case TYPE_STRING:
        fprintf(fp_B,"set %s=%s\n",entry.varname, entry.value.valStr);
        break;      
      case TYPE_FLOAT:            
        fprintf(fp_B,"set %s=%g\n",entry.varname, entry.value.valFloat);
        break;     
    }
}

void write_C(entry_t entry) {
  if (surpress_C || (opt_C==0)) return;
  switch(entry.type) {
      case TYPE_INT:
        if (entry.value.valInt<10)
          fprintf(fp_C,"#define %s %d\n",entry.varname, entry.value.valInt);        
        else
          fprintf(fp_C,"#define %s 0x%X\n",entry.varname, entry.value.valInt);
        break;      
      case TYPE_STRING:
        fprintf(fp_C,"#define %s %s\n",entry.varname, entry.value.valStr);
        break;      
      case TYPE_FLOAT:            
        fprintf(fp_C,"#define %s %g\n",entry.varname, entry.value.valFloat);
        break;     
    }
}


void parseFile(char *infilename,int opt_A,int opt_B,int opt_C) {
  char section[MAXNAMELEN+1] = "";
  char prev_name[MAXNAMELEN+1] = "";  
  FILE *fp;
  char *pos;
  int type;
  int valInt;
  char *valStr;
  float valFloat;  
  entry_t entry;
  
  if (!(fp=fopen(infilename,"r"))) error_exit("Can't open input file");
  
  linenr=0;
  while (fgets(line, sizeof(line), fp)) {
    linenr++;  
    pos=line;
    skipwhitespace(&pos);
    if ((*pos=='#') || (*pos==';')) continue;
    if (is_eol(pos)) continue;
    if (VERBOSE) printf("%s",pos);    
    entry.varname=parsevarname(&pos);
    parsePastEqual(&pos);
    type=parseentry(pos, &valInt, &valStr, &valFloat);
    entry.type=type;
    switch(type) {
      case TYPE_INT:
        entry.value.valInt = valInt;
        break;      
      case TYPE_STRING:
        entry.value.valStr = valStr;
        break;      
      case TYPE_FLOAT:            
        entry.value.valFloat = valFloat;
        break;
      default:
        error_exit("unknown type %d in line %d\n%s",linenr,line);
    }
    write_A(entry);
    write_B(entry);
    write_C(entry);        
  }
}

void open_outfiles(char *outfilepattern,int opt_A,int opt_B,int opt_C) {
  char *outfilename;
  outfilename = allocString(strlen(outfilepattern)+5);
  if (opt_A) {
     sprintf(outfilename,"%s.inc",outfilepattern);
     if (VERBOSE) printf("Writing to file %s...\n",outfilename);
     fp_A=fopen(outfilename, "w");
     if (!fp_A) error_exit("Can't open file for writing");
     fprintf(fp_A,"; *** This file was generated with %s ***\n\n",progname);          
  }
  if (opt_B) {
     sprintf(outfilename,"%s.bat",outfilepattern);
     if (VERBOSE) printf("Writing to file %s...\n",outfilename);
     fp_B=fopen(outfilename, "w");
     if (!fp_B) error_exit("Can't open file for writing");
     fprintf(fp_B,"@rem *** This file was generated with %s ***\n",progname);          
     fprintf(fp_B,"@echo off\n\n");
  }
  if (opt_C) {
     sprintf(outfilename,"%s.h",outfilepattern);
     if (VERBOSE) printf("Writing to file %s...\n",outfilename);
     fp_C=fopen(outfilename, "w");
     if (!fp_C) error_exit("Can't open file for writing");
     fprintf(fp_C,"// *** This file was generated with %s ***\n\n",progname);        
  }
}

void close_outfiles() {
  if (fp_A) fclose(fp_A);
  if (fp_B) fclose(fp_B);
  if (fp_C) fclose(fp_C);    
}


int main(int argc, char ** argv) {
  char *infilename=NULL, *outfilepattern=NULL;
  int c;
  
  progname=argv[0];
  while ((c=getopt(argc,argv,"vVABCo:?"))!=EOF)
  {
     switch(c)
     {
     case 'V':
        VERBOSE++;
        break;    
     case 'v':
        printf("%s %s\n",progname,VERSIONSTRING);
        exit(0);
        break;              
     case 'o':
     	if (outfilepattern!=NULL) usage("Only one outfile can be given.");     	
        outfilepattern=optarg;
        break;      
     case 'A':
        opt_A++;
        break;          		           
     case 'B':
        opt_B++;
        break;          		           
     case 'C':
        opt_C++;
        break;     
     case '?':
        usage(NULL);                  		           
     default:
        usage(NULL);
     }
  }
  if (opt_A+opt_B+opt_C==0) 
    usage("Please specify at least one output format.");    
  if ((opt_A>1) || (opt_B>1) || (opt_C>1) || (opt_A>1) || (VERBOSE>1))
    usage("Option specified too often.");  
  if (optind-argc==0) usage("input filename missing.");
  if (optind-argc>1) usage("you have to specify exactly one input file.");  
  infilename=argv[optind];
  if (outfilepattern==NULL) {
    int length;
    length = strlen(infilename);
    if (length>4) {
      if ( (strcmp(".ini",infilename+(length-4))==0) ||
           (strcmp(".INI",infilename+(length-4))==0) ) {
           length=length-4;
      }
    }
    outfilepattern=allocString(length);
    strncpy(outfilepattern,infilename,length); 
    outfilepattern[length]='\0';  //because strncpy does not add null terminator
  }
  
  if (VERBOSE) printf("Reading file %s...\n",infilename);
  open_outfiles(outfilepattern,opt_A,opt_B,opt_C);
  parseFile(infilename,opt_A,opt_B,opt_C);
  close_outfiles();

  return 0;
}