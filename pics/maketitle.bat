@echo off
setlocal ENABLEDELAYEDEXPANSION

set pixelshuffler="..\tools\pixelshuffler"
set scol64py="..\tools\scol64.py"

set infile="starfield.jpg"

rem Convert to Koala image
python %scol64py% --nograydither --dither 15 -p pepto --format Koala -o "starfield.kla" -c simple "%infile%"

rem Decompose koala image into color/screen and bitmap part

%pixelshuffler% -bz -p 1d73f5aec842b960 -o "titlescreen.tmp" -i "title.kla"

%pixelshuffler%  -s CpS -o "colorandscreen.tmp" -i "titlescreen.tmp"
%pixelshuffler%  -s B -o "bitmap.tmp" -i "titlescreen.tmp"

del titlescreen.tmp