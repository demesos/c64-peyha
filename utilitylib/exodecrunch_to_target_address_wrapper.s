; -------------------------------------------------------------------
; Decrunch to a given target address
; works together with exofdecrunch
; -------------------------------------------------------------------
;There are two ways to use decrunch:
;(1) by directly calling decrunch, this decrunches the data to the 
;    predefined area where it was compressed from
;(2) by loading the intended target address into AX and calling
;    decrunch_to_target_address
;
;In both cases you need to set addr_of_crunched_data first

.export _decrunch_to_target_address
.import _decrunch
.import _addr_of_crunched_data

ZP_PTR=$22

_decrunch_to_target_address:
	ldy _addr_of_crunched_data
	sty ZP_PTR 
	ldy _addr_of_crunched_data+1
	sty ZP_PTR+1
	ldy #01  ;start address is flipped, so lobyte comes second
	sta (ZP_PTR),y
	txa
	dey
	sta (ZP_PTR),y
	jmp _decrunch


