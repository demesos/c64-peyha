dialog_entry_t dialog_R1_1 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Oh, my hero!", { 9, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "I see you and the sun.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 9, 0, 0 }, { 0, 0, 0 }, "Yes, it's really hot.", { 1, 0, 0 }, { 9, 0, 0 } }, 
{ 1, { 9, 0, 0 }, { 0, 0, 0 }, "You mean son?", { 2, 0, 0 }, { 9, 0, 0 } }, 
{ 0, { 1, 0, 0 }, { 0, 0, 0 }, "No, the other sun!", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 2, 0, 0 }, { 0, 0, 0 }, "No, not son! Do you have a son?", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 2, 0, 0 }, { 0, 0, 0 }, "No, I have no son, do I?", { 3, 0, 0 }, { 2, 0, 0 } }, 
{ 1, { 2, 0, 0 }, { 0, 0, 0 }, "No, surly not.", { 3, 0, 0 }, { 2, 0, 0 } }, 
{ 0, { 3, 0, 0 }, { 0, 0, 0 }, "You have no sun. I see...", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 3, 0, 0 }, { 0, 0, 0 }, "The sun has no son.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 3, 0, 0 }, { 0, 0, 0 }, "That's the problem!", { 6, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 6, 0, 0 }, { 0, 0, 0 }, "Huh?", { 7, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 7, 0, 0 }, { 0, 0, 0 }, "Oh, god... how will this end?", { 8, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 8, 0, 0 }, { 0, 0, 0 }, "The moon? Moan? Mean? ...", { 10, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 10, 0, 0 }, { 0, 0, 0 }, "Farewell!", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R1_2 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Hello!", { 10, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 10, 0, 0 }, { 0, 0, 0 }, "Miss!", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 10, 0, 0 }, { 0, 0, 0 }, "Hi little girl!", { 1, 0, 0 }, { 10, 0, 0 } }, 
{ 1, { 10, 0, 0 }, { 0, 0, 0 }, "Go away!", { FLAG_EXIT, 0, 0 }, { 10, 0, 0 } }, 
{ 1, { 10, 0, 0 }, { 0, 0, 0 }, "Sorry, I am busy!", { 2, 0, 0 }, { 10, 0, 0 } }, 
{ 0, { 1, 0, 0 }, { 0, 0, 0 }, "What do you do here?", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 2, 0, 0 }, { 0, 0, 0 }, "Have a nice day.", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 1, 0, 0 }, { 0, 0, 0 }, "I got some business here.", { 3, 0, 0 }, { 1, 0, 0 } }, 
{ 1, { 1, 0, 0 }, { 0, 0, 0 }, "I like to have fun.", { 4, 0, 0 }, { 1, 0, 0 } }, 
{ 0, { 3, 0, 0 }, { 0, 0, 0 }, "Oh, OK miss. Take care!", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 4, 0, 0 }, { 0, 0, 0 }, "Do you want to play?", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 4, 0, 0 }, { 0, 0, 0 }, "Sure!", { 5, 0, 0 }, { 4, 0, 0 } }, 
{ 1, { 4, 0, 0 }, { 0, 0, 0 }, "Hm, maybe?", { 6, 0, 0 }, { 4, 0, 0 } }, 
{ 1, { 4, 0, 0 }, { 0, 0, 0 }, "Nah, not now.", { 7, 0, 0 }, { 4, 0, 0 } }, 
{ 0, { 5, 0, 0 }, { 0, 0, 0 }, "Let's play gonu.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 5, 0, 0 }, { 0, 0, 0 }, "Thanks. Until next time.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 7, 0, 0 }, { 0, 0, 0 }, "Bye bye...", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 6, 0, 0 }, { 0, 0, 0 }, "Look what I painted!", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 6, 0, 0 }, { 0, 0, 0 }, "Do you like it?", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 6, 0, 0 }, { 0, 0, 0 }, "Yes, I like it.", { 8, 0, 0 }, { 6, 0, 0 } }, 
{ 1, { 6, 0, 0 }, { 0, 0, 0 }, "Yes, I love it.", { 9, 0, 0 }, { 6, 0, 0 } }, 
{ 0, { 8, 0, 0 }, { 0, 0, 0 }, "Thank you. See you!", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 9, 0, 0 }, { 0, 0, 0 }, "You can have it.", { 91, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 9, 0, 0 }, { 0, 0, 0 }, "Have fun miss!", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R1_3 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Welcome! Sale! Sale!", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 0, 0 }, { 0, 0, 0 }, "Hi.", { 7, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 7, 0, 0 }, { 0, 0, 0 }, "What can I do for you?", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 7, 0, 0 }, { 0, 0, 0 }, "Where am I?", { 1, 0, 0 }, { 7, 0, 0 } }, 
{ 1, { 7, 0, 0 }, { 0, 0, 0 }, "What do you sell?", { 2, 0, 0 }, { 7, 0, 0 } }, 
{ 0, { 1, 0, 0 }, { 0, 0, 0 }, "Are you kidding me?", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 2, 0, 0 }, { 0, 0, 0 }, "Many things. Everything ", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 2, 0, 0 }, { 0, 0, 0 }, "you can see here ...", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 2, 0, 0 }, { 0, 0, 0 }, "How about this?", { 3, 0, 0 }, { 2, 0, 0 } }, 
{ 1, { 2, 0, 0 }, { 0, 0, 0 }, "How about that?", { 4, 0, 0 }, { 2, 0, 0 } }, 
{ 0, { 3, 0, 0 }, { 0, 0, 0 }, "This is the moon.", { 5, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 3, 0, 0 }, { 0, 0, 0 }, "It reprents to queen.", { 5, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 4, 0, 0 }, { 0, 0, 0 }, "This is the sun.", { 5, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 4, 0, 0 }, { 0, 0, 0 }, "It reprents our emperor.", { 5, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 5, 0, 0 }, { 0, 0, 0 }, "You want to buy it?", { 6, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 6, 0, 0 }, { 0, 0, 0 }, "Oh, I am out of money.", { FLAG_EXIT, 0, 0 }, { 6, 0, 0 } }, 
{ 1, { 6, 0, 0 }, { 0, 0, 0 }, "Maybe next time.", { FLAG_EXIT, 0, 0 }, { 6, 0, 0 } }
};

dialog_entry_t dialog_R1_4 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Hi.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 0, 0 }, { 0, 0, 0 }, "Hi.", { 1, 0, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R2_1 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Hey..", { 9, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 9, 0, 0 }, { 0, 0, 0 }, "Miss.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 9, 0, 0 }, { 0, 0, 0 }, "Hi, something off, kid?", { 1, 0, 0 }, { 9, 0, 0 } }, 
{ 1, { 9, 0, 0 }, { 0, 0, 0 }, "Hi! Have a nice day!", { 2, 0, 0 }, { 9, 0, 0 } }, 
{ 0, { 1, 0, 0 }, { 0, 0, 0 }, "Huh? Oh, I wanted to...", { 3, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 2, 0, 0 }, { 0, 0, 0 }, "Thanks, you too!", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 3, 0, 0 }, { 0, 0, 0 }, "..eat? I may have some-", { 4, 0, 0 }, { 3, 0, 0 } }, 
{ 1, { 3, 0, 0 }, { 0, 0, 0 }, "..join the kids here?", { 5, 0, 0 }, { 3, 0, 0 } }, 
{ 1, { 3, 0, 0 }, { 0, 0, 0 }, "...?", { 6, 0, 0 }, { 3, 0, 0 } }, 
{ 0, { 4, 0, 0 }, { 0, 0, 0 }, "No, ya got me wrong! Nevermind.", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 5, 0, 0 }, { 0, 0, 0 }, "Ye, but they don't talk to me!", { 7, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 6, 0, 0 }, { 0, 0, 0 }, "Ah, sorry! Have a nice day!", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 7, 0, 0 }, { 0, 0, 0 }, "Oh, I am sorry for that.", { 11, 0, 0 }, { 7, 0, 0 } }, 
{ 1, { 7, 0, 0 }, { 0, 0, 0 }, "I can help you with that!", { 8, 0, 0 }, { 7, 0, 0 } }, 
{ 0, { 8, 0, 0 }, { 0, 0, 0 }, "Thanks. Bye.", { 50, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 11, 0, 0 }, { 0, 0, 0 }, "Bye.", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R2_2 [] = {
{ 0, { 50, 0, 0 }, { 0, 0, 0 }, "Don't distract us please!", { FLAG_EXIT, 51, 1 }, { 50, 0, 0 } }, 
{ 0, { 51, 0, 0 }, { 1, 0, 0 }, "I got no time!", { FLAG_EXIT, 52, 9 }, { 51, 0, 0 } }, 
{ 0, { 52, 0, 0 }, { 0, 0, 9 }, "Alright, what do you want?", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 52, 0, 0 }, { 0, 0, 9 }, "That kid wants to join.", { 1, 0, 0 }, { 9, 0, 0 } }, 
{ 1, { 52, 0, 0 }, { 0, 0, 9 }, "Oh nothing, sorry!", { 2, 0, 0 }, { 9, 0, 0 } }, 
{ 0, { 52, 1, 0 }, { 0, 0, 9 }, "Really? Hmm...", { 7, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 52, 1, 0 }, { 0, 0, 9 }, "Ask my friend!", { FLAG_EXIT, 60, 0 }, { 0, 0, 0 } }, 
{ 0, { 52, 2, 0 }, { 0, 0, 9 }, "Alright!", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 0, 0, 0 }, { 50, 51, 52 }, "Don't distract us please!", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 0, 0, 0 }, { 50, 51, 52 }, "Come back later!", { 0, 0, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R2_3 [] = {
{ 0, { 0, 0, 0 }, { 60, 0, 0 }, "Not now.", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 60, 0, 0 }, { 0, 0, 7 }, "Oh, Miss...", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 60, 0, 0 }, { 0, 0, 7 }, "Mind the girl to join?", { 1, 7, 0 }, { 0, 0, 0 } }, 
{ 1, { 60, 0, 0 }, { 0, 0, 7 }, "How are you?", { 2, 7, 0 }, { 0, 0, 0 } }, 
{ 0, { 60, 1, 0 }, { 0, 0, 0 }, "Sorry, we prefer to play alone.", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 60, 2, 0 }, { 0, 0, 0 }, "Fine.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 60, 2, 0 }, { 0, 0, 0 }, "This is for you.", { 3, 0, 0 }, { 2, 0, 0 } }, 
{ 1, { 60, 2, 0 }, { 0, 0, 0 }, "This is for you two.", { 3, 0, 0 }, { 2, 0, 0 } }, 
{ 1, { 60, 2, 0 }, { 0, 0, 0 }, "This is for you three.", { 4, 0, 0 }, { 2, 0, 0 } }, 
{ 0, { 60, 3, 0 }, { 0, 0, 0 }, "Thank you so much!", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 60, 4, 0 }, { 0, 0, 0 }, "Thank you. You are generous.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 60, 4, 0 }, { 0, 0, 0 }, "By the way: I think you you want to go into the palace, right? There is a secret entrance to the right.", { 92, 0, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R2_4 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Stop! ", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "You can't pass!", { 0, 0, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R2_5 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Stay away!", { 0, 0, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R3_1 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Hello.", { 1, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 1, 0, 0 }, { 0, 0, 0 }, "Hello. I want to pass.", { 2, 0, 0 }, { 1, 0, 0 } }, 
{ 1, { 1, 0, 0 }, { 0, 0, 0 }, "Let me through!", { 2, 0, 0 }, { 1, 0, 0 } }, 
{ 0, { 2, 0, 0 }, { 0, 0, 0 }, "Talk to the scribe next to me.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 2, 0 }, { 0, 0, 0 }, "Okay bye.", { FLAG_EXIT, 0, 0 }, { 2, 0, 0 } }, 
{ 1, { 40, 2, 0 }, { 0, 0, 0 }, "He won't let me pass.", { 3, 0, 0 }, { 2, 0, 0 } }, 
{ 1, { 40, 2, 0 }, { 0, 0, 0 }, "He requires papers.", { 3, 0, 0 }, { 2, 0, 0 } }, 
{ 0, { 0, 3, 0 }, { 0, 0, 0 }, "So show him.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 3, 0 }, { 0, 0, 0 }, "I don't have them.", { 4, 0, 0 }, { 3, 0, 0 } }, 
{ 0, { 0, 4, 0 }, { 0, 0, 0 }, "Go get the papers then.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 4, 0 }, { 0, 0, 0 }, "They are inside the walls.", { 5, 0, 0 }, { 4, 0, 0 } }, 
{ 0, { 0, 5, 0 }, { 0, 0, 0 }, "I see your problem.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 5, 0 }, { 0, 0, 0 }, "Can you help me?", { 6, 0, 0 }, { 5, 0, 0 } }, 
{ 0, { 0, 6, 0 }, { 0, 0, 0 }, "Hmm. Tell him you have a permit pending.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 6, 0 }, { 0, 0, 0 }, "What does that mean?", { 7, 41, 0 }, { 6, 0, 0 } }, 
{ 0, { 0, 7, 0 }, { 0, 0, 0 }, "Just tell him that okay?", { FLAG_EXIT, 0, 0 }, { 7, 0, 0 } }, 
{ 0, { 42, 0, 0 }, { 0, 0, 0 }, "What did I tell you?", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 42, 0, 0 }, { 0, 0, 0 }, "Thanks for helping!", { 8, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 8, 0, 0 }, { 0, 0, 0 }, "You are welcome. Go on through.", { FLAG_EXIT, 0, 0 }, { 8, 0, 0 } }
};

dialog_entry_t dialog_R3_2 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Papers please.", { 1, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 1, 0, 0 }, { 41, 0, 0 }, "I don't have papers.", { 2, 0, 0 }, { 1, 0, 0 } }, 
{ 1, { 1, 0, 0 }, { 41, 0, 0 }, "What papers?", { 2, 0, 0 }, { 1, 0, 0 } }, 
{ 0, { 2, 0, 0 }, { 0, 0, 0 }, "No papers, no passage. Good bye.", { 3, 0, 0 }, { 2, 0, 0 } }, 
{ 1, { 3, 0, 0 }, { 41, 0, 0 }, "I wasn't finished.", { 4, 0, 0 }, { 3, 0, 0 } }, 
{ 0, { 4, 0, 0 }, { 0, 0, 0 }, "Clearly you were. You asked, I said no.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 4, 0, 0 }, { 0, 0, 0 }, "It is urgent!", { 5, 0, 0 }, { 4, 0, 0 } }, 
{ 1, { 4, 0, 0 }, { 0, 0, 0 }, "I can pay for passage!", { 5, 0, 0 }, { 4, 0, 0 } }, 
{ 1, { 4, 0, 0 }, { 0, 0, 0 }, "The king is in danger!", { 5, 0, 0 }, { 4, 0, 0 } }, 
{ 0, { 5, 0, 0 }, { 0, 0, 0 }, "No papers, no passage, no exceptions.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 5, 0, 0 }, { 0, 0, 0 }, "Where can i get papers?", { 6, 0, 0 }, { 5, 0, 0 } }, 
{ 0, { 6, 0, 0 }, { 0, 0, 0 }, "In the city hall.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 6, 0, 0 }, { 0, 0, 0 }, "Great where is that?", { 7, 0, 0 }, { 6, 0, 0 } }, 
{ 0, { 7, 0, 0 }, { 0, 0, 0 }, "Inside these walls. Good bye.", { FLAG_EXIT, 40, 0 }, { 7, 0, 0 } }, 
{ 1, { 1, 41, 0 }, { 0, 0, 0 }, "I have a permit pending.", { 8, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 8, 0, 0 }, { 0, 0, 0 }, "Rigth. Show me your proof of application.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 8, 0, 0 }, { 0, 0, 0 }, "I don't have it.", { 9, 0, 0 }, { 8, 0, 0 } }, 
{ 1, { 8, 0, 0 }, { 0, 0, 0 }, "I didn't get it.", { 9, 0, 0 }, { 8, 0, 0 } }, 
{ 0, { 9, 0, 0 }, { 0, 0, 0 }, "No worries. It should be at the archives.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 9, 0, 0 }, { 0, 0, 0 }, "Let me guess...", { 10, 0, 0 }, { 9, 0, 0 } }, 
{ 1, { 9, 0, 0 }, { 0, 0, 0 }, "Where is that?", { 10, 0, 0 }, { 9, 0, 0 } }, 
{ 0, { 10, 0, 0 }, { 0, 0, 0 }, "Through the gate and to the left.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 10, 0, 0 }, { 0, 0, 0 }, "Do you see my problem?", { 11, 0, 0 }, { 10, 0, 0 } }, 
{ 1, { 10, 0, 0 }, { 0, 0, 0 }, "So let me in.", { 11, 0, 0 }, { 10, 0, 0 } }, 
{ 0, { 11, 0, 0 }, { 0, 0, 0 }, "No.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 11, 0, 0 }, { 21, 0, 0 }, "I need papers.", { 12, 0, 0 }, { 11, 0, 0 } }, 
{ 1, { 11, 0, 0 }, { 22, 0, 0 }, "I need proof.", { 13, 0, 0 }, { 11, 0, 0 } }, 
{ 1, { 11, 0, 0 }, { 23, 0, 0 }, "I need permits.", { 14, 0, 0 }, { 11, 0, 0 } }, 
{ 0, { 12, 0, 0 }, { 0, 0, 0 }, "Yes.", { 15, 0, 0 }, { 12, 0, 0 } }, 
{ 0, { 13, 0, 0 }, { 0, 0, 0 }, "Yes.", { 16, 0, 0 }, { 13, 0, 0 } }, 
{ 0, { 14, 0, 0 }, { 0, 0, 0 }, "Yes.", { 17, 0, 0 }, { 14, 0, 0 } }, 
{ 1, { 15, 0, 0 }, { 0, 0, 0 }, "...to get papers.", { 18, 0, 0 }, { 15, 0, 0 } }, 
{ 1, { 16, 0, 0 }, { 0, 0, 0 }, "...to get proof.", { 19, 0, 0 }, { 16, 0, 0 } }, 
{ 1, { 17, 0, 0 }, { 0, 0, 0 }, "...to get permits.", { 20, 0, 0 }, { 17, 0, 0 } }, 
{ 0, { 18, 0, 0 }, { 0, 0, 0 }, "Yes...", { 21, 11, 0 }, { 18, 0, 0 } }, 
{ 0, { 19, 0, 0 }, { 0, 0, 0 }, "Yes...", { 22, 11, 0 }, { 19, 0, 0 } }, 
{ 0, { 20, 0, 0 }, { 0, 0, 0 }, "Yes.....?", { 23, 11, 0 }, { 20, 0, 0 } }, 
{ 1, { 21, 22, 23 }, { 0, 0, 0 }, "Do you not see?", { 24, 0, 0 }, { 21, 22, 23 } }, 
{ 0, { 24, 0, 0 }, { 0, 0, 0 }, "Oh my... The system... It's broken...", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 24, 0, 0 }, { 0, 0, 0 }, "It's okay.", { 25, 0, 0 }, { 24, 0, 0 } }, 
{ 1, { 24, 0, 0 }, { 0, 0, 0 }, "Nothing is perfect.", { 25, 0, 0 }, { 24, 0, 0 } }, 
{ 0, { 25, 0, 0 }, { 0, 0, 0 }, "I need to fix it. Find a sollution..", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 25, 0, 0 }, { 0, 0, 0 }, "So.. I may pass?", { 26, 0, 0 }, { 25, 0, 0 } }, 
{ 0, { 26, 0, 0 }, { 0, 0, 0 }, "Yes, yes! Just get out of my sight.", { FLAG_EXIT, 93, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R3_3 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Looks you are new here.", { 1, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 1, 0, 0 }, { 0, 0, 0 }, "On whose side are? On the kings or on the ministers side?", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 1, 0, 0 }, { 0, 0, 0 }, "On the kings of course!", { 2, 0, 0 }, { 1, 0, 0 } }, 
{ 1, { 1, 0, 0 }, { 0, 0, 0 }, "The Minister is fine.", { 3, 0, 0 }, { 1, 0, 0 } }, 
{ 1, { 1, 0, 0 }, { 0, 0, 0 }, "I am neutral.", { 4, 0, 0 }, { 1, 0, 0 } }, 
{ 0, { 2, 0, 0 }, { 0, 0, 0 }, "So shall it be.", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 3, 0, 0 }, { 0, 0, 0 }, "Interesting.", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 4, 0, 0 }, { 0, 0, 0 }, "Make up your mind!", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R4_1 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Halt, who goes there?", { 1, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 1, 0, 0 }, { 0, 0, 0 }, "Let me pass please!", { 2, 0, 0 }, { 1, 0, 0 } }, 
{ 1, { 1, 0, 0 }, { 0, 0, 0 }, "I require passage!", { 2, 0, 0 }, { 1, 0, 0 } }, 
{ 0, { 2, 0, 0 }, { 0, 0, 0 }, "Ofcourse. What's the password?", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 2, 0, 0 }, { 0, 0, 0 }, "Administrator?", { 3, 0, 0 }, { 2, 0, 0 } }, 
{ 1, { 2, 0, 0 }, { 0, 0, 0 }, "1234abcd?", { 3, 0, 0 }, { 2, 0, 0 } }, 
{ 1, { 2, 0, 0 }, { 0, 0, 0 }, "Please?", { 3, 0, 0 }, { 2, 0, 0 } }, 
{ 1, { 2, 70, 0 }, { 0, 0, 0 }, "Admiral.", { 4, 0, 0 }, { 2, 0, 0 } }, 
{ 0, { 3, 0, 0 }, { 0, 0, 0 }, "Incorrect. Good bye.", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 4, 0, 0 }, { 0, 0, 0 }, "Correct. You may pass.", { 0, 94, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R4_2 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Let me do my work!", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R4_3 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "And the wizard said to the beast: Let me pass! To which the beast replied NAY. And the wizard spoke the arcane words.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 0, 0 }, { 0, 0, 0 }, "Keep Listening.", { 2, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 0, 0 }, { 0, 0, 0 }, "That's too much. Bye.", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 0, 0, 2 }, { 0, 0, 0 }, "The beast screeched and stared angrily at the wizard, who repeated words: My magic compelles you! Let me pass. ", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 0, 2 }, { 0, 0, 0 }, "Keep Listening.", { 3, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 0, 0, 3 }, { 0, 0, 0 }, "Allright, wizard, you win this one. Answer me this riddle and go in peace. Who commands the wooden walls floating at sea?", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 0, 3 }, { 0, 0, 0 }, "Keep Listening.", { 4, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 0, 0, 4 }, { 0, 0, 0 }, "And the wizard replied: That's too easy! Admiral is the answer! and the beast disapeard in a puff of smoke.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 0, 4 }, { 0, 0, 0 }, "Leave.", { FLAG_EXIT, 70, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 0, 4 }, { 0, 0, 0 }, "Thank you for the story.", { FLAG_EXIT, 70, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R5_1 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Work is getting harder and harder.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "I feel I have to double work these days.", { 0, 0, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R5_2 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "You did it!", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "You reached me - the minister - in the center of the palace.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Who would have thought?", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "However, you won't get to the king!", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 0, 0, 0 }, { 0, 0, 0 }, "So all was for nothing?", { 1, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 1, 0, 0 }, { 0, 0, 0 }, "Maybe.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 1, 0, 0 }, { 0, 0, 0 }, "Maybe not.", { 0, 0, 0 }, { 0, 0, 0 } }
};

dialog_entry_t dialog_R5_3 [] = {
{ 0, { 0, 0, 0 }, { 0, 0, 0 }, "Hello..", { 7, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 7, 0, 0 }, { 0, 0, 0 }, "..Miss!", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 7, 0, 0 }, { 0, 0, 0 }, "Hello.", { 1, 0, 0 }, { 7, 0, 0 } }, 
{ 1, { 7, 0, 0 }, { 0, 0, 0 }, "Mam!", { 2, 0, 0 }, { 7, 0, 0 } }, 
{ 0, { 1, 0, 0 }, { 0, 0, 0 }, "Please take care.", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 2, 0, 0 }, { 0, 0, 0 }, "Strange things go on here.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 2, 0, 0 }, { 0, 0, 0 }, "I am totally confused.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 2, 0, 0 }, { 0, 0, 0 }, "Don't worry. I am here.", { 3, 0, 0 }, { 2, 0, 0 } }, 
{ 1, { 2, 0, 0 }, { 0, 0, 0 }, "How this comes?", { 4, 0, 0 }, { 2, 0, 0 } }, 
{ 0, { 3, 0, 0 }, { 0, 0, 0 }, "Thanks for your support, miss.", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 4, 0, 0 }, { 0, 0, 0 }, "I think I've bad eyes. I see things twice. I just thought I saw the king here, then he was there.. I am so confused. Don't tell anyone please, so I will not get fired.", { 0, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 4, 0, 0 }, { 0, 0, 0 }, "All right, don't worry.", { 5, 0, 0 }, { 0, 0, 0 } }, 
{ 1, { 4, 0, 0 }, { 0, 0, 0 }, "It maybe was too hot. ", { 5, 0, 0 }, { 0, 0, 0 } }, 
{ 0, { 5, 0, 0 }, { 0, 0, 0 }, "Yes, miss.", { FLAG_EXIT, 0, 0 }, { 0, 0, 0 } }
};

