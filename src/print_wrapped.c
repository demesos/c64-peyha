/****************************************************************
 * Wrapped printing into a given window
 * by Wil
 * License CC BY 4.0
 ****************************************************************/
 
#include "presets.h"
#include <frodosC64lib.h>
#include "print_wrapped.h"

#include <string.h>
#include <conio.h>

int lenword(char *text)
{
   int l=0;
   while((*text != 0) && (*text != 13) && (*text != 32)) {
     l++;
     text++;
   }
   return l;
}

#define chrout(c) (__AX__ = (c),asm("jsr $FFD2"))

void print_wrapped_windowed(char *text, uint8_t x1, uint8_t y1, uint8_t width, uint8_t height, char endchar) {
  int rest,l;
  uint8_t right_margin;
  
  right_margin = 40-x1-width;
  
  while(*text!=0) {
    rest=80-peek(211);
    if (rest>40) rest-=40;
    
    rest-=right_margin;

    l=lenword(text);
    if (l>rest) { //does the next word not fit into current line?
      chrout(13);
      poke(211,x1);
    }
    if (l>0) {  //do we have a word with length>0 to print?
      while((*text != 0) && (*text != 13) && (*text != 32)) {
        chrout(*text);
        text++;      
      } 
      rest=80-peek(211);
      if (rest>40) rest-=40;    
      rest-=right_margin;
      
      if (rest==0) {
        chrout(13);
        poke(211,x1);
        rest=width;
      }
      
      if (rest == width)
        while(*text == 32) text++; //skip spaces after entering a new line 
    }    
    if (*text == 13) {
      chrout(*text);
      poke(211,x1);
      text++;     
    } else if (*text == 32) {
      chrout(*text);
      text++;     
    }
  }

  if (endchar) {
    rest=80-peek(211);
    if (rest>40) rest-=40;    
    rest-=right_margin;

    switch(endchar) {
    case 13:
        chrout(13);
        poke(211,x1);    
        break;
    case 32:
        if ((rest==0) || (rest==width)) break;
        // fallthrough!
    default:
        if (rest==0) {
          chrout(13);
          poke(211,x1);          
        }
        chrout(endchar);
    }
  }
}