/****************************************************************
 * header file for the game
 * by Wil
 * License CC BY 4.0
 ****************************************************************/

#ifndef _MAIN_H_

#define _MAIN_H_

#include <stdint.h>     // uint*_t

/* --------------------------------------------------
   all functions that are defined after they are 
   called need to be declared here
   so that the compiler knows how they are called
   -------------------------------------------------- */
void init();
void menuscreen();
void clrhalfscreen();

#endif