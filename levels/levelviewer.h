/****************************************************************
 * header file for C Wrappers of levelviewer funtion
 * by Wil
 * License CC BY 4.0
 ****************************************************************/

extern void __fastcall__ init_map(); //initializes color and charset
extern void __fastcall__ draw_map(); //draws map from position in registers X and Y

//#define draw_map_macro(X,Y) (     asm("jsr %v",draw_map ) )

                                  
                                   
#define draw_map_macro(X,Y) (     poke(PASSX,X), \
                                  poke(PASSY,X), \
                                  asm("jsr %v",draw_map ) )
 